package com.example.androidlab.ui.map

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.androidlab.R
import com.example.androidlab.databinding.FragmentMapBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.squareup.moshi.Moshi
import org.json.JSONObject

class MapFragment : Fragment(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private var locationPermissionGranted: Boolean = false
    private lateinit var currentLocation: Location
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var lastKnownLocation: Location? = null

    private lateinit var binding: FragmentMapBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapBinding.inflate(inflater, container, false)
        val root = binding.root

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())

        return root
    }

    override fun onStart() {
        super.onStart()
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        sendWeatherRequest()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        getLocationPermission()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSION_REQUEST_ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                    updateLocationUI()
                    getDeviceLocation()
                }
            }
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
            updateLocationUI()
            getDeviceLocation()
        } else {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun updateLocationUI() {
        if (mMap == null) {
            return
        }
        try {
            if (locationPermissionGranted) {
                mMap?.isMyLocationEnabled = true
                mMap?.uiSettings?.isMyLocationButtonEnabled = true
                mMap?.setOnMyLocationButtonClickListener {
                    getDeviceLocation()
                    true
                }
            } else {
                mMap?.isMyLocationEnabled = false
                mMap?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("MapFragment","Exception ${e.message}")
        }
    }

    private fun getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            mMap?.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    LatLng(
                                        lastKnownLocation!!.latitude,
                                        lastKnownLocation!!.longitude
                                    ), DEFAULT_ZOOM
                                )
                            )
                        }
                    } else {
                        mMap?.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM)
                        )
                        mMap?.uiSettings?.isMyLocationButtonEnabled = false
                    }
                    sendWeatherRequest()
                }
            }
        } catch (e: SecurityException) {
            Log.e("MapFragment", "Exception: ${e.message}")
        }
    }

    private fun sendWeatherRequest() {
        val queue = Volley.newRequestQueue(requireContext())

        val cnt = 1
        val apiKey = "505922b85f89fada73bd879779768d52"
        val lat: Double = lastKnownLocation?.latitude ?: DEFAULT_LOCATION.latitude
        val lon: Double = lastKnownLocation?.longitude ?: DEFAULT_LOCATION.longitude
        val units = "metric"
        val lang = "ru"

        val url = "http://api.openweathermap.org/data/2.5/find?lat=$lat&lon=$lon&cnt=$cnt&units=$units&lang=$lang&appid=$apiKey"
        Log.d("MapFragment", "url = $url")

        val request = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            { handleWeatherResponse(it) },
            { Log.e("MapFragment", "Exception: $it") }
        )

        queue.add(request)
    }

    private fun handleWeatherResponse(response: JSONObject) {
        Log.d("MapFragment", "Response: $response")

        val weatherAdapter = Moshi.Builder().build().adapter(WeatherJson::class.java)
        val weatherObject = weatherAdapter.fromJson(response.toString())

        if (weatherObject != null) {
            if (!weatherObject.list.isNullOrEmpty()) {
                weatherObject.list.firstOrNull()?.let { cityInfo ->
                    cityInfo.main?.temp?.let { temp ->
                        binding.tvTemp.text = if (temp > 0.0) {
                            "+$temp °C"
                        } else {
                            "$temp °C"
                        }
                    }
                    cityInfo.weather?.firstOrNull()?.description?.let { weatherDesc ->
                        binding.tvDesc.text = weatherDesc.replaceFirstChar { char ->
                            char.uppercaseChar()
                        }
                    }
                }
            }
        }
    }


    companion object {
        private const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 1
        const val DEFAULT_ZOOM = 12.0F
        val DEFAULT_LOCATION = LatLng(55.99, 92.79)
    }

}