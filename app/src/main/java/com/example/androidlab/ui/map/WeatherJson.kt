package com.example.androidlab.ui.map


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WeatherJson(
    @Json(name = "cod")
    val cod: String?,
    @Json(name = "count")
    val count: Int?,
    @Json(name = "list")
    val list: List<City>?,
    @Json(name = "message")
    val message: String?
) {
    @JsonClass(generateAdapter = true)
    data class City(
        @Json(name = "clouds")
        val clouds: Clouds?,
        @Json(name = "coord")
        val coord: Coord?,
        @Json(name = "dt")
        val dt: Int?,
        @Json(name = "id")
        val id: Int?,
        @Json(name = "main")
        val main: Main?,
        @Json(name = "name")
        val name: String?,
        @Json(name = "rain")
        val rain: Any?,
        @Json(name = "snow")
        val snow: Any?,
        @Json(name = "sys")
        val sys: Sys?,
        @Json(name = "weather")
        val weather: List<Weather?>?,
        @Json(name = "wind")
        val wind: Wind?
    ) {
        @JsonClass(generateAdapter = true)
        data class Clouds(
            @Json(name = "all")
            val all: Int?
        )

        @JsonClass(generateAdapter = true)
        data class Coord(
            @Json(name = "lat")
            val lat: Double?,
            @Json(name = "lon")
            val lon: Double?
        )

        @JsonClass(generateAdapter = true)
        data class Main(
            @Json(name = "humidity")
            val humidity: Int?,
            @Json(name = "pressure")
            val pressure: Int?,
            @Json(name = "temp")
            val temp: Double?,
            @Json(name = "temp_max")
            val tempMax: Double?,
            @Json(name = "temp_min")
            val tempMin: Double?
        )

        @JsonClass(generateAdapter = true)
        data class Sys(
            @Json(name = "country")
            val country: String?
        )

        @JsonClass(generateAdapter = true)
        data class Weather(
            @Json(name = "description")
            val description: String?,
            @Json(name = "icon")
            val icon: String?,
            @Json(name = "id")
            val id: Int?,
            @Json(name = "main")
            val main: String?
        )

        @JsonClass(generateAdapter = true)
        data class Wind(
            @Json(name = "deg")
            val deg: Int?,
            @Json(name = "gust")
            val gust: Int?,
            @Json(name = "speed")
            val speed: Double?
        )
    }
}